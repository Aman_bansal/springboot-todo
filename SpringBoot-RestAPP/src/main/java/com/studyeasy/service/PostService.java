package com.studyeasy.service;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

import com.studyeasy.entity.Post;

@Service
public class PostService {
	
	static List<Post> posts = new ArrayList<>(Arrays.asList(
			                                                 new Post(1, "Hamstring", "legWorkout"),
			                           		                 new Post(2, "Squats", "legWorkout"),
			                           		                 new Post(3, "Biceps", "ARM Workout")
			                           		                    ));
	public List<Post> getPosts()
	{
		return posts;
	}
	public Post getPost(int id) {
		
		for(Post temp : posts)
		{
			if(temp.getPostID() == id)
			{
				return temp;
			}
		}
		return null;
	}

	public void addElement(Post element) {
	posts.add(element);
		
	}
	public void updatePost(Post post, int id) {
		for(int i=0;i<posts.size();i++)
		{
			Post temp=posts.get(i);
			if(temp.getPostID() == id)
			{
			posts.set(i, post);
			}
		}
		
	}
	public void deletePost(int id) {
	for(int i=0;i<posts.size();i++)
	{
		Post temp =posts.get(i);
		if(temp.getPostID() == id)
		{
			posts.remove(temp);
		}
	}
		
	}
	

}
